# Shell C
Développement d'un shell Linux avec toutes les fonctionnalités en C from scratch.
> GIT : https://gitlab.com/cours-l3/projet-mini-shell

# SETUP 
Allez dans les sources
> cd shell-Clair_Boureux/src
Build les sources
> cmake ../. && cmake --build .
Lancement du shell
> ./minishell

# INFO 
Le worker CI ne marche plus depuis la migration de make vers cmake.

Pas le temps de refaire la configuration. Les tests unitaires on était abandonnés en même temps.


# Tache
- De se déplacer dans les répertoires :
* [x] cd [dir]

- D’exécuter des commandes simple, en "avant plan" et en "arrière plan :
* [x] commande
* [x] commande &
* [x] commande1 ; commande2 ; commande3 ...

- De rediriger les entrées sorties des commandes :
* [x] commande > file
* [x] commande 2> file
* [x] commande < file
* [x] commande >> file
* [x] commande 2>> file
* [x] commande >&2
* [x] commande 2&1

- De gérer les variables d’environnement :
* [x] export VAR=ma_value
* [x] unset VAR

- De gérer l'exit
* [x] exit 42

- De gérer les opérateurs
* [x] ! commande
* [x] commande && commande ...
* [x] commande || commande ...

## Cas particulier
Le pwd fonctionné déjà sur mon shell.
* [x] pwd
Le les variables d'environnement fonctionné déjà sur mon shell.
* [x] echo $VAR *//Gestion des variables d'environnement : le Shell effectue automatiquement la substitution des variables d'environnement.*

## Problémes
* [ ] commande | commande
Le pipe ne fonctionne pas totalement un ou plusieurs tube n'est pas fermé. Du coup la lercture ne s'arrête jamais fonction avec :
> head less cat...

