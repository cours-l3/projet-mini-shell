#ifndef PROCESS_H
#define PROCESS_H

#include <sys/types.h>
#include "parsing.h"

typedef struct process{
  char* path;
  char** argv;
  int stdin, stdout, stderr;
  int status;
  pid_t pid;
  int background;
  int inv;
  int * tube;
  struct process * next;
  struct process * next_success;
  struct process * next_failed;
} process_t;

int exec_process(process_t * proc);
int print_process(process_t * proc);

#endif
