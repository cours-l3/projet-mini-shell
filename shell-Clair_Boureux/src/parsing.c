/*
 * Fichier : parsing.c
 * Auteur : CLAIR Thomas
 * Dependances : parsfing.h
 * Description :    Gestion de string et tableau de string en tous genre 
 *		    Nettoyage, decoupage, parsing, split...
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "parsing.h"

/*
 * "Nettoyer" la chaîne de caractères
 * Trim les blanc à gauche et à droite
 */
char* clean_str(char* str) {
    assert(str!=NULL);
    assert(strlen(str)<MAXSTRSIZE);

    // Supprimer les blancs au début et à la fin de la ligne
    // Ex: "   str1    str2    " => "str1    str2"
    str = trimn(str);
    str = trim_left(str);
    str = trim_right(str);
//    printf(str);

    // Supprimer les doublons d'espaces entre les mots de la ligne
    // Ex: "str1    str2  str3" => "str1 str2 str3"

    return str;
}


/**
 * Fonction qui supprime les \n
 */
char *trimn (char *s) {
    int i = strlen(s)-1;
    if ((i > 0) && (s[i] == '\n'))
	s[i] = '\0';
    return s;
}

// Supp les espaces dans str à gauche
// str char* chaîne à trim
// retourn un char* avec la chaîne trimé
char* trim_left(char *str){
    //	printf("%u",*it);
    //avance le pointeur de 1 si c'est un espace
    for(;*str == 32;str++){}
    return str;
}

// Supp les char dans str à droite d'un =
// str char* chaîne à trim
// retourn un char* avec la chaîne trimé
char* split_right(char* str){
    char* original = str + strlen(str);
    while(*--original != '=');
    *(original) = '\0';
    return str;
}

// Supp les char dans str à gauche jusqu'au =
// str char* chaîne à trim
// retourn un char* avec la chaîne trimé
char* split_left(char *str){
    //	printf("%u",*it);
    //avance le pointeur de 1 si c'est pas un =
    for(;*str != 61;str++){}
    str++;
    return str;
}

// Supp les espaces dans str à droite
// str char* chaîne à trim
// retourn un char* avec la chaîne trimé
char* trim_right(char* str){
    char* original = str + strlen(str);
    while(*--original == ' ');
    *(original + 1) = '\0';
    return str;
}




// Cherche toutes les chaînes du tableau commençant par '$'
// Récupère la variable d'environnement correspondante et
// la substitue à l'élément du tableau
// Attention: si la variable n'est pas définie on utilisera
// la chaîne vide "" (et pas NULL)

// Renvoie le nombre de variables substituées
int set_envs(char* tokens[]) {
    assert(tokens!=NULL);


    int n=0;
    for(char** tok=tokens; *tok!=NULL; ++tok){
	if((*tok)[0]=='$'){
	    char * value=getenv((*tok)+1);
	    if(value==NULL){  
		*tok="";
	    }else{
		*tok=value;
		n++;
	    }
	}
    }
    return n;
}


// Découper la chaîne de caractères en ses différents éléments
size_t split_str(char * str, char * tokens[]){
    //"cmd opt1 opt2; cmd2"-->{"cmd","opt1","opt2",";",NULL}
    tokens[0]= str;
    size_t idx_tok = 1,str_size = strlen(str);
    for(size_t i = 0 ; i < str_size; ++i){
	if(str[i] == ' '){
	    str[i]='\0';
	    idx_tok++;
	    tokens[idx_tok-1]=str+i+1;
	}
    }
    tokens[idx_tok]=NULL;
    // Renvoie le nombre d'éléments dans le tableau tokens
    return idx_tok;
}
