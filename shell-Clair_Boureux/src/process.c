/*
 * Fichier : process.c
 * Auteur : CLAIR Thomas
 * Dependances : process.h
 * Description :    Execute un processus 
 *		    Affiche un processus
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

#include "process.h"

/* 
 * Lance un nouveau processus qui exécute le programme passé en paramètre
 * dans la structure proc. La fonction attend la fin de l'exécution si
 * proc.background != 0. Les entrées/sorties sont redirigées si nécessaire.
 * Le PID du nouveau processus est stocké dans proc.pid. À la fin de l'exécution
 * proc.status récupère le status de la commande tel que retourné par waitpid()
 */
int exec_process(process_t * proc)
{

    //wait the end if proc.background != 0
    pid_t cpid; 
    if ((proc->pid=fork()) == 0) {
	//Redirige la sortie standards vers stdout
	if( proc->stdout != 1 ){
	    dup2(proc->stdout,1);
	    if(proc->stdout != 0 && proc->stdout !=2)
		close(proc->stdout);
	}
	//Redirige la sortie erreur vers stderr
	if( proc->stderr != 2 ){
	    dup2(proc->stderr,2);
	    if(proc->stderr != 0 && proc->stderr !=1)
		close(proc->stderr);
	}
	//Redirige la sortie input standard vers stdin
	if( proc->stdin != 0 ){
	    dup2(proc->stdin,0);
	    if(proc->stdin != 2 && proc->stdin !=1)
		close(proc->stdin);
	}

	//execution du prog
	execvp(proc->path,proc->argv);

	exit(0);           /* terminate child */
    }
    int result=WEXITSTATUS(proc->status);

    if( proc->background != 0 ){
//	printf("\04\r\n\0");
	waitpid(proc->pid,&proc->status,0); /* reaping parent */
//	printf("EXECPROC : Parent pid = %d\n", getpid()); 
//	printf("EXECPROC : pid status= %d\n", proc->status); 
///	printf("EXECPROC : Child pid = %d\n", proc->pid); 

	if(proc->inv == 1){
	    if(result != 0)
		result = 0;
	    else if(result == 0)
		result = 1;
	}
	return result;
    }

    if(proc->inv == 1){
	if(result != 0)
	    result = 0;
	else if(result == 0)
	    result = 1;
    }
//    printf("EXECPROC : Parent pid = %d\n", getpid()); 
//    printf("EXECPROC : pid status= %d\n", proc->status); 
//    printf("EXECPROC : Child pid = %d\n", proc->pid); 


    return result;
}

/**
 * Affiche toute les informations sur un process
 */
int print_process(process_t * p){
    printf("\n\n");
    printf("	Path : %s\n",p->path);
    printf("	Argv : %s\n",*p->argv);
    printf("	Stdin : %u\n",p->stdin);
    printf("	Stdout : %u\n",p->stdout);
    printf("	Stderr : %u\n",p->stderr);
    printf("	pid_t : %u\n",(int)p->pid);
    printf("	background : %u\n",p->background);
    printf("	Inv : %u\n",p->inv);
    if(p->next != NULL){
	printf("{next\n");
	print_process(p->next);
	printf("}\n");
    }
    if(p->next_success != NULL){
	printf("{next_success\n");
	print_process(p->next_success);
	printf("}\n");
    }
    if(p->next_failed != NULL){
	printf("{next_failed\n");
	print_process(p->next_failed);
	printf("}\n");
    }
}

