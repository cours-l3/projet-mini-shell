#ifndef PARSING_H
#define PARSING_H

#define MAXSTRSIZE 2048
#define MAXCMD     128

char* clean_str(char* str);
char* trimn(char* str);
char* trim_right(char* str);
char* trim_left(char* str);
char* split_right(char* str);
char* split_left(char* str);
size_t split_str(char* str, char* tokens[]);
int set_envs(char* tokens[]);

#endif

