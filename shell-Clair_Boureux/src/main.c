/*
 * Fichier : main.c
 * Auteur : CLAIR Thomas
 * Dependances : parsing.h process.h
 *
 *     _      _      _
 *  __(.)< __(.)> __(.)=
 *  \___)  \___)  \___)   
 *~~~~~~~~~~~~~~~~~~~~~~~
 * Une fois j'ai vu un canard. Il a cassé 3 pattes à une brique.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "parsing.h"
#include "process.h"

//Variable pour faire des | 
int fun[2];

int parse_cmds(char** , process_t * );


//Pour les test
//{"ls", "-l",";","cat","test.txt",">","out",";","ls","&&","echo","ok",";","||","echo","KO",NULL }
int main(int argc, char * argv[]){
    char cmdline[MAXSTRSIZE];
    char * cmds[MAXCMD];
    process_t procs[MAXCMD];
    cmdline[MAXSTRSIZE-1]=0;

    while(1){
	process_t * procs = malloc(sizeof(process_t) * MAXCMD);
	for(size_t i=0; i< MAXCMD ; i++){
	    procs[i].path == NULL;


	    procs[i].stdin=0;
	    procs[i].stdout=1;
	    procs[i].stderr=2;

	    procs[i].background=1;
	    procs[i].inv=0;
	    procs[i].path=cmds[i];
	    procs[i].argv=cmds+i;
	}

	printf("=================\n");
	printf("->$ ");
	fgets(cmdline,MAXSTRSIZE,stdin);
	printf("=================\n");

	clean_str(cmdline);
	split_str(cmdline,cmds);
	set_envs(cmds);
	parse_cmds(cmds, procs);


	process_t * p = procs;
	while(p != NULL){
	    //print_process(p);
	    //printf("path : %s\n",p->argv);
	    int result = exec_process(p);
	    if(p->background == 0){
		if(p->next_success != NULL)
		    p = (process_t *) p->next_success;
		if(p->next_failed != NULL)
		    p = (process_t *) p->next_failed;
		p = (process_t *) p->next;
		continue;
	    }
	    if(p->next_success != NULL)
		if(result == 0){
		    p = (process_t *) p->next_success;
		    continue;
		}

	    if(p->next_failed != NULL)
		if(result != 0){
		    p = (process_t *) p->next_failed;
		    continue;
		}
	    p = (process_t *) p->next;
	}
    }
}



/**
 * Detecte les elements qui doivent etre interpretes par le shell.
 * Effectue les modification necessaire sur la structure processus
 */
int parse_cmds(char* cmds[], process_t * procs){
    size_t idx=0;
    int i=0;
    procs[idx].path = cmds[i];
    procs[idx].argv = cmds+i;
    while(cmds[i] != NULL){
	//	printf("args : %s\n", cmds[i]);
	if(strcmp(cmds[i], "|") == 0) {
	    if(pipe(fun)==-1)
		exit(8);
	    procs[idx+1].tube = fun;
	    procs[idx].tube = fun;

	    procs[idx].stdout = procs[idx].tube[1];
	    procs[idx+1].stdin = procs[idx].tube[0];

	    procs[idx].next = (process_t *) procs+idx+1;
	    procs[idx+1].path = *(cmds+i+1);
	    procs[idx+1].argv = cmds+i + 1;

	    cmds[i] = NULL;
	    idx+2;
	    i+2;
	    continue;
	}
	if(strcmp(cmds[i], "<") == 0) {
	    cmds[i] = NULL;
	    procs[idx].stdin = open((cmds[i+1]), O_RDONLY, 0);
	    idx+2;
	    i+2;
	    continue;
	}
	if(strcmp(cmds[i],"!") ==0){
	    procs[idx].inv = 1;
	    cmds[i]=NULL;
	    procs[idx].path = *(cmds+i+1);
	    procs[idx].argv = cmds+i + 1;
	    //idx++;
	    i++;
	    continue;
	}
	if(strcmp(cmds[i],"export") == 0){
	    char name[MAXSTRSIZE];
	    char value[MAXSTRSIZE];
	    strcpy(name, cmds[i+1]);
	    strcpy(value, cmds[i+1] );

	    setenv(split_right(name),split_left(value),1);

	    cmds[i]=NULL;
	    cmds[i+1]=NULL;
	    idx+2;
	    i+2;
	    continue;
	}

	if(strcmp(cmds[i],"unset") == 0){
	    unsetenv(cmds[i+1]);
	    printf("VAR %s unset", cmds[i+1]);
	    cmds[i]=NULL;
	    cmds[i+1]=NULL;
	    idx+2;
	    i+2;
	    continue;
	}
	if(strcmp(cmds[i],"cd") == 0){
	    chdir(cmds[i+1]);
	    cmds[i]=NULL;
	    cmds[i+1]=NULL;
	    idx+2;
	    i+2;
	    continue;
	}
	if(strcmp(cmds[i],"exit") == 0){
	    if(cmds[i+1] != NULL)
		exit(atoi( cmds[i+1]));
	    else
		exit(0);
	    cmds[i]=NULL;
	    cmds[i+1]=NULL;
	    idx+2;
	    i+2;
	    continue;
	}
	if(strcmp(cmds[i],";") == 0){
	    cmds[i]=NULL;
	    procs[idx].next = (process_t *) procs+idx+1;
	    procs[idx+1].path = *(cmds+i+1);
	    procs[idx+1].argv = cmds+i + 1;
	    ++idx;
	    ++i;
	    continue;
	}
	if(strcmp(cmds[i],"&") == 0){
	    cmds[i]=NULL;
	    procs[idx].background = 0;
	    ++idx;
	    ++i;
	    continue;
	}
	if(strcmp(cmds[i],"&&") == 0){
	    cmds[i]=NULL;
	    procs[idx].next_success = (process_t *) procs+idx+1;
	    procs[idx+1].path = *(cmds+i+1);
	    procs[idx+1].argv = cmds + i + 1;
	    ++idx;
	    ++i;
	    continue;
	}
	if(strcmp(cmds[i],"||") == 0){
	    cmds[i]=NULL;
	    procs[idx].next_failed = (process_t *) procs+idx+1;
	    procs[idx+1].path = *(cmds+i+1);
	    procs[idx+1].argv = cmds + i + 1;
	    ++idx;
	    ++i;
	    continue;
	}
	if(strcmp(cmds[i], ">") == 0){
	    procs[idx].stdout = open(cmds[i+1], O_RDWR | O_CREAT | O_TRUNC , S_IRUSR| S_IWUSR);
	    if(procs[idx].stdout == -1)
		return -1;
	    cmds[i]=NULL;
	    cmds[i+1]=NULL;
	    idx+2;
	    i+2;
	    continue;
	}
	if(strcmp(cmds[i],">>") == 0){
	    procs[idx].stdout = open(cmds[i+1], O_RDWR | O_CREAT | O_APPEND , S_IRUSR| S_IWUSR);
	    if(procs[idx].stdout == -1)
		return -1;
	    cmds[i]=NULL;
	    cmds[i+1]=NULL;
	    idx+2;
	    i+2;
	    continue;
	}
	if(strcmp(cmds[i],"2>") == 0){
	    procs[idx].stderr = open(cmds[i+1], O_RDWR | O_CREAT | O_TRUNC , S_IRUSR| S_IWUSR);
	    if(procs[idx].stderr == -1)
		return -1;
	    cmds[i]=NULL;
	    cmds[i+1]=NULL;
	    idx+2;
	    i+2;
	    continue;
	}
	if(strcmp(cmds[i],"2>>") == 0){
	    procs[idx].stderr = open(cmds[i+1], O_RDWR | O_CREAT | O_APPEND , S_IRUSR| S_IWUSR);
	    if(procs[idx].stderr == -1)
		return -1;
	    cmds[i]=NULL;
	    cmds[i+1]=NULL;
	    idx+2;
	    i+2;
	    continue;
	}
	if(strcmp(cmds[i],">&2") == 0 || strcmp(cmds[i],"1>&2") == 0){
	    procs[idx].stdout = 2;
	    cmds[i]=NULL;
	    idx++;
	    i++;
	    continue;
	}
	if(strcmp(cmds[i],"2>&1") == 0){
	    procs[idx].stderr = 1;
	    cmds[i]=NULL;
	    idx++;
	    i++;
	    continue;
	}
	if(strcmp(cmds[i], "<") == 0) {
	    cmds[i] = NULL;
	    procs[idx].stdin = open((cmds[i+1]), O_RDONLY, 0);
	    ++cmds;
	    ++i;
	    continue;
	}
	i++;
    }
}


