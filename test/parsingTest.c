#include "CUnit/Basic.h"
//open file
#include <sys/stat.h>

#include "../parsing.h"
#include "../process.h"

void split_strTest(){
    char cmdline[MAXSTRSIZE];
    char * cmds[MAXCMD];
    cmdline[MAXSTRSIZE-1]=0;

    char * test =strdup( "1 2 3 4");
    char * tokens[MAXCMD];
    split_str(test,tokens);
    int i = 1;
    char result[50];
    for(char ** tok =tokens;*tok!=NULL;++tok){
	sprintf(result,"%u", i);
	CU_ASSERT_STRING_EQUAL(result,*tok);
	i++;
    }

}

void exec_processTest(){
	process_t proc;

	char * argv_proc[] = {"echo", "coucou"};

	proc.path = argv_proc[0];
	proc.argv = argv_proc;
	proc.stdin = 0; 
	proc.stdout = 1;
	proc.stderr = 2;
	//0 to do in BG
	proc.background = 1;

	//--------
	exec_process(&proc);//lance ls -l et continue
	CU_ASSERT_STRING_EQUAL("","");
};	

//char* trim_left(char* str);
//char* trim_right(char* str);
void trim_leftTest() {
	// Check if first param matches with second[2]
	CU_ASSERT_STRING_EQUAL(trim_left("   azerty"), "azerty");
	CU_ASSERT_STRING_EQUAL(trim_left("     a a a "), "a a a ");
	CU_ASSERT_STRING_EQUAL(trim_left("       "), "");
}

void trim_rightTest() {
	// Check if first param matches with second[2]
	char azerty[]  = "azerty   ";
	CU_ASSERT_STRING_EQUAL(trim_right(azerty), "azerty");
	char aaa[] = " a a a     ";
	CU_ASSERT_STRING_EQUAL(trim_right(aaa), " a a a");
	char blanc[] = "         ";
	CU_ASSERT_STRING_EQUAL(trim_right(blanc), "");
}

void trim_clean_strTest() {
	// Check if first param matches with second[2]
	char azerty[]  = "      azerty   ";
	CU_ASSERT_STRING_EQUAL(clean_str(azerty), "azerty");
	char aaa[] = "a a a     ";
	CU_ASSERT_STRING_EQUAL(clean_str(aaa), "a a a");
	char bbb[] = "     b b b";
	CU_ASSERT_STRING_EQUAL(clean_str(bbb), "b b b");
	char blanc[] = "         ";
	CU_ASSERT_STRING_EQUAL(trim_right(blanc), "");
}

int main() {
	// Initialize the CUnit test registry
	if (CUE_SUCCESS != CU_initialize_registry())
		return CU_get_error();

	// Sets the basic run mode, CU_BRM_VERBOSE will show maximum output of run details
	// Other choices are: CU_BRM_SILENT and CU_BRM_NORMAL
	CU_basic_set_mode(CU_BRM_VERBOSE);

	CU_pSuite pSuite = NULL;

	// Add a suite to the registry
	pSuite = CU_add_suite("Parsing_test_suite", 0, 0);

	// Always check if add was successful
	if (NULL == pSuite) {
		CU_cleanup_registry();
		return CU_get_error();
	}

	// Add the test to the suite
	if (NULL == CU_add_test(pSuite, "left_trim_test", trim_leftTest) ||
			CU_add_test(pSuite, "right_trim_test", trim_rightTest) == NULL || 
			CU_add_test(pSuite, "clean_str_test", trim_clean_strTest) == NULL ||
			CU_add_test(pSuite, "split_str_test", split_strTest) == NULL 

	   ) { 
		CU_cleanup_registry();
		return CU_get_error();
	}

	// Run the tests and show the run summary
	CU_basic_run_tests();
	return CU_get_number_of_failures();
}

